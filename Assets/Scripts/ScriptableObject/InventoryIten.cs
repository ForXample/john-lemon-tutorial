﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Key Types", menuName = "Keys", order = 1)]
public class InventoryIten : ScriptableObject
{
    [SerializeField] private string itemName = "New item";
    [SerializeField] private string itemDescription = "This is an old rusty key. I wonder which door it leads to.";
    [SerializeField] public Texture2D itemIcon = null;
    [SerializeField] public GameObject itemObject;

}
