﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    [Header("UI Components")]
    public Text textLabel;

    [Header("Text Files")]
    public TextAsset textFile;
    public int index;

    public GameObject objectiveSystem;

    List<string> textList = new List<string>();


    void Awake()
    {
        GetTextFromFile(textFile);

    }

    private void OnEnable()
    {
        textLabel.text = textList[index];
        index++;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && index == textList.Count)
        {
            gameObject.SetActive(false);

            objectiveSystem.SetActive(true);
            index = 0;
            return;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            textLabel.text = textList[index];
            index++;
        }
    }

    void GetTextFromFile(TextAsset file)
    {
        textList.Clear();
        index = 0;

        var lineData = file.text.Split('\n');

        foreach (var line in lineData)
        {
            textList.Add(line);

        }

    }
}
